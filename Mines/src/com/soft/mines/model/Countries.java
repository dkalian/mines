package com.soft.mines.model;

/**
 * @author Nickolai
 *
 */
public enum Countries {

	abkhazia("Абхазия"), afghanistan("Афганистан"), aland("Аландские острова"), albania(
			"Албания"), algeria("Алжир"), american_samoa("Американское самоа"), andorra(
			"Андорра"), angola("Ангола"), anguilla("Ангилия"), antarctica(
			"Антарктида"), antigua_and_barbuda("Антигуа и Барбуда"), argentina(
			"Аргентина"), armenia("Армения"), aruba("Аруба"), australia(
			"Австралия"), austria("Австрия"), azerbaijan("Азейбарджан"), bahamas(
			"Багамские острова"), bahrain("Бахрейн"), bangladesh("Бангладеш"), barbados(
			"Барбадос"), basque_country("Страна Басков"), belarus("Беларусь"), belgium(
			"Бельгия"), belize("Белиз"), benin("Бенин"), bermuda("Бермуды"), bhutan(
			"Бутан"), bolivia("Боливия"), bosnia_and_herzegovina(
			"Босния и Герцеговина"), botswana("Ботсвана"), brazil("Бразилия"), british_antarctic_territory(
			"Британская антарктическая территория"), british_virgin_islands(
			"Британские Виргинские острова"), brunei("Бруней"), bulgaria(
			"Болгария"), burkina_faso("Буркина-Фасо"), burundi("Бурунди"), cambodia(
			"Камбоджа"), cameroon("Камерун"), canada("Канада"), canary_islands(
			"Канарские острова"), cape_verde("Кабо-Верде"), cayman_islands(
			"Каймановы острова"), central_african_republic(
			"Центральноафриканская Республика"), chad("Чад"), chile("Чили"), china(
			"Китайская Народная Республика"), christmas_island(
			"Остров Рождества"), cocos_keeling_islands("Кокосовые острова"), colombia(
			"Колумбия"), commonwealth("Содружество наций"), comoros("Коморы"), cook_islands(
			"Острова Кука"), costa_rica("Коста-Рика"), cote_divoire(
			"Кот-д’Ивуар"), croatia("Хорватия"), cuba("Куба"), curacao(
			"Кюрасао"), cyprus("Кипр"), czech_republic("Чехия"), democratic_republic_of_the_congo(
			"Демократическая Республика Конго"), denmark("Дания"), djibouti(
			"Джибути"), dominica("Доминика"), dominican_republic(
			"Доминиканская Республика"), east_timor("Восточный Тимор"), ecuador(
			"Эквадор"), egypt("Египет"), el_salvador("Сальвадор"), england(
			"Англия"), equatorial_guinea("Экваториальная Гвинея"), eritrea(
			"Эритрея"), estonia("Эстония"), ethiopia("Эфиопия"), european_union(
			"Европейский союз"), falkland_islands("Фолклендские острова"), faroes(
			"Фарерские острова"), fiji("Фиджи"), finland("Финляндия"), france(
			"Франция"), french_polynesia("Французская Полинезия"), french_southern_territories(
			"Французские Южные и Антарктические территории"), gabon("Габон"), gambia(
			"Гамбия"), georgia("Грузия"), germany("Германия"), ghana("Гана"), gibraltar(
			"Гибралтар"), gosquared(""), greece("Греция"), greenland(
			"Гренландия"), grenada("Гренада"), guam("Гуам"), guatemala(
			"Гватемала"), guernsey("Гернси"), guinea("Гвинея"), guinea_bissau(
			"Гвинея-Бисау"), guyana("Гайана"), haiti("Республика Гаити"), honduras(
			"Гондурас"), hong_kong("Гонконг"), hungary("Венгрия"), iceland(
			"Исландия"), india("Индия"), indonesia("Индонезия"), iran("Иран"), iraq(
			"Ирак"), ireland("Ирландия"), isle_of_man("Остров Мэн"), israel(
			"Израиль"), italy("Италия"), jamaica("Ямайка"), japan("Япония"), jersey(
			"Джерси"), jordan("Иордания"), kazakhstan("Казахстан"), kenya(
			"Кения"), kiribati("Кирибати"), kosovo("Республика Косово"), kuwait(
			"Кувейт"), kyrgyzstan("Киргизия"), laos("Лаос"), latvia("Латвия"), lebanon(
			"Ливан"), lesotho("Лесото"), liberia("Либерия"), libya("Ливия"), liechtenstein(
			"Лихтенштейн"), lithuania("Литва"), luxembourg("Люксембург"), macau(
			"Макао"), macedonia("Республика Македония"), madagascar(
			"Мадагаскар"), malawi("Малави"), malaysia("Малайзия"), maldives(
			"Мальдивы"), mali("Мали"), malta("Мальта"), mars("Марс"), marshall_islands(
			"Маршалловы Острова"), martinique("Мартиника"), mauritania(
			"Мавритания"), mauritius("Маврикий"), mayotte("Майотта"), mexico(
			"Мексика"), micronesia("Микронезия"), moldova("Молдавия"), monaco(
			"Монако"), mongolia("Монголия"), montenegro("Черногория"), montserrat(
			"Монтсеррат"), morocco("Марокко"), mozambique("Мозамбик"), myanmar(
			"Мьянма"), nagorno_karabakh("Нагорно-Карабахская Республика"), namibia(
			"Намибия"), nato("НАТО"), nauru("Науру"), nepal("Непал"), netherlands(
			"Нидерланды"), netherlands_antilles(
			"Нидерландские Антильские острова"), new_caledonia(
			"Новая Каледония"), new_zealand("Новая Зеландия"), nicaragua(
			"Никарагуа"), niger("Нигер"), nigeria("Нигерия"), niue("Ниуэ"), norfolk_island(
			"Норфолк"), northern_cyprus("Турецкая Республика Северного Кипра"), northern_mariana_islands(
			"Турецкая Республика Северного Кипра"), north_korea(
			"Корейская Народно-Демократическая Республика"), norway("Норвегия"), olympics(
			""), oman("Оман"), pakistan("Пакистан"), palau("Палау"), palestine(
			"Палестина"), panama("Панама"), papua_new_guinea(
			"Папуа - Новая Гвинея"), paraguay("Парагвай"), peru("Перу"), philippines(
			"Филиппины"), pitcairn_islands("Острова Питкэрн"), poland("Польша"), portugal(
			"Португалия"), puerto_rico("Пуэрто-Рико"), qatar("Катар"), red_cross(
			"Красный крест"), republic_of_the_congo("Республика Конго"), romania(
			"Румыния"), russia("Россия"), rwanda("Руанда"), saint_barthelemy(
			"Сен-Бартелеми"), saint_helena("Остров Святой Елены"), saint_kitts_and_nevis(
			"Сент-Китс и Невис"), saint_lucia("Сент-Люсия"), saint_martin(
			"Сен-Мартен"), saint_vincent_and_the_grenadines(
			"Сент-Винсент и Гренадины"), samoa("Самоа"), san_marino(
			"Сан-Марино"), sao_tome_and_principe("Сан-Томе и Принсипи"), saudi_arabia(
			"Саудовская Аравия"), scotland("Шотландия"), senegal("Сенегал"), serbia(
			"Сербия"), seychelles("Сейшельские Острова"), sierra_leone(
			"Сьерра-Леоне"), singapore("Сингапур"), slovakia("Словакия"), slovenia(
			"Словения"), solomon_islands("Соломоновы Острова"), somalia(
			"Сомали"), somaliland("Сомалиленд"), south_africa(
			"Южно-Африканская Республика"), south_georgia_and_the_south_sandwich_islands(
			"Южная Георгия и Южные Сандвичевы острова"), south_korea(
			"Республика Корея"), south_ossetia("Южная Осетия"), south_sudan(
			"Южный Судан"), spain("Испания"), sri_lanka("Шри-Ланка"), sudan(
			"Судан"), suriname("Суринам"), swaziland("Свазиленд"), sweden(
			"Швеция"), switzerland("Швейцария"), syria("Сирия"), taiwan(
			"Китайская Республика"), tajikistan("Таджикистан"), tanzania(
			"Танзания"), thailand("Таиланд"), togo("Того"), tokelau("Токелау"), tonga(
			"Тонга"), trinidad_and_tobago("Тринидад и Тобаго"), tunisia("Тунис"), turkey(
			"Турция"), turkmenistan("Туркмения"), turks_and_caicos_islands(
			"Теркс и Кайкос"), tuvalu("Тувалу"), uganda("Уганда"), ukraine(
			"Украина"), united_arab_emirates(
			"Административное деление Объединённых Арабских Эмиратов"), united_kingdom(
			"Великобритания"), united_nations("ООН"), united_states(
			"Соединённые Штаты Америки"), unknown("Не известно"), uruguay(
			"Уругвай"), us_virgin_islands("Виргинские острова"), uzbekistan(
			"Узбекистан"), vanuatu("Вануату"), vatican_city("Ватикан"), venezuela(
			"Венесуэла"), vietnam("Вьетнам"), wales("Уэльс"), wallis_and_futuna(
			"Уоллис и Футуна"), western_sahara("Западная Сахара"), yemen(
			"Йемен"), zambia("Замбия"), zimbabwe("Зимбабве");

	public String countryName;

	Countries(String countryName) {
		this.countryName = countryName;
	}
}
