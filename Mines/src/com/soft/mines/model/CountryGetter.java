package com.soft.mines.model;

/**
 * @author Nickolai
 *
 */
public class CountryGetter {

	/**
	 * @param fileName имя файла
	 * @return Stirng имя страны на русском языке
	 */
	public static String getCountry(String fileName) {
		
		fileName = fileName.replaceAll(".png", "");
		System.out.println(fileName);
		
		String country = null;
		for (Countries countriesElement : Countries.values()) {
			if (countriesElement.name().compareTo(fileName) == 0) { 
				return countriesElement.countryName;
			}
		}
		return country;
	}
}
