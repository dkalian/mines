package com.soft.mines.model.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.GetResources;
import com.soft.mines.model.constants.ConstantsSQL;

/**
 * @author Doc
 * 
 */
public class MineAdapter extends BaseAdapter implements Filterable {
	private static final int IMAGE_MINE = R.id.textImageInfo;
	private static final int NAME_MINE = R.id.nameMine;
	private static final int NAME_TEXT = R.id.mineText;
	private static final int MINE_ID = R.id.mineId;
	private static final int IMAGE_COUNTRY = R.id.imageMineInfoContry;
	private static final int RESOURCE = R.layout.mine_list_view_layout;
	private ViewHolder holder;
	private Context context;
	private List<HashMap<String, String>> dataMap;
	
	/**
	 * @param context
	 * @param dataMap
	 */
	public MineAdapter(Context context, List<HashMap<String, String>> dataMap) {
		this.context = context;
		if (this.dataMap == null) {
			this.dataMap = dataMap;
		}
		
	}

	static class ViewHolder {
		public ImageView image_mine;
		public TextView name_mine;
		public TextView text_mine;
		public TextView id_mine;
		public ImageView image_country;

	}

	@Override
	public int getCount() {
		return dataMap.size();
	}

	@Override
	public Object getItem(int position) {
		return dataMap.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(RESOURCE, null);
			holder = new ViewHolder();
			holder.image_mine = (ImageView) v.findViewById(IMAGE_MINE);
			holder.name_mine = (TextView) v.findViewById(NAME_MINE);
			holder.text_mine = (TextView) v.findViewById(NAME_TEXT);
			holder.image_country = (ImageView) v.findViewById(IMAGE_COUNTRY);
			holder.id_mine = (TextView) v.findViewById(MINE_ID);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		HashMap<String, String> mineMap = new HashMap<String, String>();
		mineMap = (HashMap<String, String>) dataMap.get(position);
		if (mineMap.get(ConstantsSQL.IMAGE1).length() > 0 && mineMap.get(ConstantsSQL.COUNTRY).length() > 0) {
			holder.image_country.setImageDrawable(GetResources.getDrawable(context,
					String.valueOf(mineMap.get(ConstantsSQL.COUNTRY))));
			holder.image_mine.setImageDrawable(GetResources.getDrawable(context,
					String.valueOf(mineMap.get(ConstantsSQL.IMAGE1))));
		}
		holder.name_mine.setText(mineMap.get(ConstantsSQL.NAME));
		holder.text_mine.setText(mineMap.get(ConstantsSQL.MINE_INFO));
		holder.id_mine.setText(mineMap.get(ConstantsSQL.ID));
		return v;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults filterResults) {
				dataMap = (List<HashMap<String, String>>) filterResults.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				List<HashMap<String, String>> filterResults = new ArrayList<HashMap<String, String>>();
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					filterResults = FilterSearch(constraint, dataMap);
					results.values = filterResults;
					results.count = filterResults.size();
				} else {
					results.values = dataMap;
					results.count = dataMap.size();
				}
				return results;
			}
		};
	}

	private List<HashMap<String, String>> FilterSearch(CharSequence charSequence, List<HashMap<String, String>> rawData) {
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		for (HashMap<String, String> data : rawData) {
			if (String.valueOf(data.get(ConstantsSQL.NAME)).toUpperCase(Locale.ENGLISH)
					.contains(charSequence.toString().toUpperCase(Locale.ENGLISH))) {
				result.add(data);
			}
		}
		return result;
	}
}
