package com.soft.mines.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.mines.R;

/**
 * @author Doc
 * @version 0.0.1
 */
public class MenuAdapter extends BaseAdapter {

	private static final int MENUTEXT = R.id.textMenu;
	private static final int MENUTEXTABOUT = R.id.textMenuAbout;
	private static final int MENUIMG = R.id.imageMenu;
	private static final int RESOURCE = R.layout.menu_main_list_view_layout;
	private static final int[] text = { R.string.main_menu_item1, R.string.main_menu_item2, R.string.main_menu_item3,
			R.string.main_menu_item4 };
	private static final int[] text_about = { R.string.main_menu_item1_about, R.string.main_menu_item2_about,
			R.string.main_menu_item3_about, R.string.main_menu_item4_about };

	private static final int[] img = { R.drawable.ic_action_protivotank, R.drawable.ic_action_protivopehot,
			R.drawable.ic_action_country, R.drawable.ic_action_testing };
	private Context context;

	/**
	 * @param titleMenu
	 * @param sitesList
	 * @param context
	 */
	public MenuAdapter(Context context) {
		this.context = context;

	}

	@Override
	public int getCount() {
		return text.length;
	}

	@Override
	public Object getItem(int position) {
		return text[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private TextView textTitle;
	private ImageView imageMenu;
	private TextView textAbout;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(RESOURCE, null);
		}
		textTitle = (TextView) v.findViewById(MENUTEXT);
		textAbout = (TextView) v.findViewById(MENUTEXTABOUT);
		imageMenu = (ImageView) v.findViewById(MENUIMG);
		textTitle.setText(text[position]);
		textAbout.setText(text_about[position]);
		imageMenu.setImageResource(img[position]);
		return v;
	}

}
