package com.soft.mines.model.SQL;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.soft.mines.model.GetResources;
import com.soft.mines.model.constants.Constants;
import com.soft.mines.model.constants.ConstantsSQL;

/**
 * @author Doc
 * 
 */
public class SQLAdapter {

	private SQLiteDatabase database;
	private SQLConnectionHelper dbHelper;
	private Context context;

	/**
	 * @param context
	 * @throws SQLException
	 *             <h1>��� �������� Activity ����������� ������� ����� close()
	 */
	public SQLAdapter(Context context) throws SQLException {
		this.context = context;
		dbHelper = new SQLConnectionHelper(context);
		database = dbHelper.getWritableDatabase();

	}

	/**
	 * @return ���������� ArrayList<HashMap<String, String>> �� ����� ����������
	 *         ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getAllData() {
		Cursor cursor = database.query(ConstantsSQL.TABLE_MINE, null, null, null, null, null, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * @return ���������� ArrayList<HashMap<String, String>> � ����������
	 *         Constants.MINES_TANK ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getTanksMinesData() {
		Cursor cursor = database.query(ConstantsSQL.TABLE_MINE, null, ConstantsSQL.MINE_TYPE + " = ?",
				new String[] { Constants.MINES_TANK }, null, null, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * @return ���������� ArrayList<HashMap<String, String>> � ����������
	 *         Constants.MINES_SOLDATS ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getSoldatsMinesData() {
		Cursor cursor = database.query(ConstantsSQL.TABLE_MINE, null, ConstantsSQL.MINE_TYPE + " = ?",
				new String[] { Constants.MINES_SOLDATS }, null, null, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * @param country 
	 * @return ���������� ArrayList<HashMap<String, String>> � ���������� �� ������
	 *         country ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getAllMinesCountry(String country) {
		Cursor cursor = database.query(ConstantsSQL.TABLE_MINE, null, ConstantsSQL.COUNTRY + " = ?",
				new String[] { country }, null, null, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * @param id
	 * @return ���������� ArrayList<HashMap<String, String>> � ����������
	 *         Constants.MINES_TANK ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getOneMineInfo(String id) {
		Cursor cursor = database.query(ConstantsSQL.TABLE_MINE, null, ConstantsSQL.ID + " = ?", new String[] { id },
				null, null, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * @param id
	 * @return ���������� ArrayList<HashMap<String, String>> � �����������
	 *         �������� �� ���� ConstantsSQL.COUNTRY ������� TABLE_MINE
	 */
	public ArrayList<HashMap<String, String>> getCounnryMinesSort() {
		Cursor cursor = database.query(true, ConstantsSQL.TABLE_MINE, null, null, null, ConstantsSQL.COUNTRY, null,
				ConstantsSQL.COUNTRY, null);
		return getMinesForAdapter(cursor);

	}

	/**
	 * ��������� ����������� � ����
	 */
	public void close() {
		if (database != null)
			dbHelper.close();
	}

	private ArrayList<HashMap<String, String>> getMinesForAdapter(Cursor cursor) {
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> temp = new HashMap<String, String>();
				temp.put(ConstantsSQL.ID, String.valueOf(cursor.getInt(cursor.getColumnIndex(ConstantsSQL.ID))));
				temp.put(ConstantsSQL.COUNTRY, cursor.getString(cursor.getColumnIndex(ConstantsSQL.COUNTRY)));
				temp.put(ConstantsSQL.MINE_TYPE, cursor.getString(cursor.getColumnIndex(ConstantsSQL.MINE_TYPE)));
				temp.put(ConstantsSQL.MINE_TTX, cursor.getString(cursor.getColumnIndex(ConstantsSQL.MINE_TTX)));
				temp.put(ConstantsSQL.IMAGE1, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE1)));
				temp.put(ConstantsSQL.IMAGE2, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE2)));
				temp.put(ConstantsSQL.IMAGE3, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE3)));
				temp.put(ConstantsSQL.IMAGE4, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE4)));
				temp.put(ConstantsSQL.IMAGE5, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE5)));
				temp.put(ConstantsSQL.IMAGE6, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE6)));
				temp.put(ConstantsSQL.IMAGE7, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE7)));
				temp.put(ConstantsSQL.IMAGE8, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE8)));
				temp.put(ConstantsSQL.IMAGE9, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE9)));
				temp.put(ConstantsSQL.IMAGE10, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE10)));
				temp.put(ConstantsSQL.IMAGE_TEST, cursor.getString(cursor.getColumnIndex(ConstantsSQL.IMAGE_TEST)));

				temp.put(ConstantsSQL.MINE_INFO, GetResources.getString(context,
						cursor.getString(cursor.getColumnIndex(ConstantsSQL.MINE_INFO))));
				temp.put(ConstantsSQL.NAME,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.NAME))));
				temp.put(ConstantsSQL.TEXT1,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT1))));
				temp.put(ConstantsSQL.TEXT2,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT2))));
				temp.put(ConstantsSQL.TEXT3,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT3))));
				temp.put(ConstantsSQL.TEXT4,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT4))));
				temp.put(ConstantsSQL.TEXT5,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT5))));
				temp.put(ConstantsSQL.TEXT6,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT6))));
				temp.put(ConstantsSQL.TEXT7,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT7))));
				temp.put(ConstantsSQL.TEXT8,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT8))));
				temp.put(ConstantsSQL.TEXT9,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT9))));
				temp.put(ConstantsSQL.TEXT10,
						GetResources.getString(context, cursor.getString(cursor.getColumnIndex(ConstantsSQL.TEXT10))));
				result.add(temp);
			} while (cursor.moveToNext());
		} else
			cursor.close();
		return result;
	}
}
