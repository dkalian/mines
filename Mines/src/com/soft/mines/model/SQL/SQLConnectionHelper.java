package com.soft.mines.model.SQL;

import com.soft.mines.model.constants.ConstantsSQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Doc ����� ����������� � ���� ������
 */
public class SQLConnectionHelper extends SQLiteOpenHelper {
	private Context context;

	/**
	 * @param context
	 * @param name
	 * @param factory
	 * @param version
	 */
	public SQLConnectionHelper(Context context) {
		super(context, ConstantsSQL.DATABASE_NAME, null, ConstantsSQL.DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		onCreateWidget(db);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	private void onCreateWidget(SQLiteDatabase db) {
		db.execSQL("create table " + ConstantsSQL.TABLE_MINE + " (" + ConstantsSQL.ID
				+ " integer primary key autoincrement, " + ConstantsSQL.IMAGE1 + " text, " + ConstantsSQL.IMAGE2
				+ " text, " + ConstantsSQL.IMAGE3 + " text, " + ConstantsSQL.IMAGE4 + " text, " + ConstantsSQL.IMAGE5
				+ " text, " + ConstantsSQL.IMAGE6 + " text, " + ConstantsSQL.IMAGE7 + " text, " + ConstantsSQL.IMAGE8
				+ " text, " + ConstantsSQL.IMAGE9 + " text, " + ConstantsSQL.IMAGE10 + " text, "
				+ ConstantsSQL.IMAGE_TEST + " text, " + ConstantsSQL.NAME + " text, " + ConstantsSQL.COUNTRY
				+ " text, " + ConstantsSQL.MINE_TYPE + " text, " + ConstantsSQL.MINE_INFO + " text, "
				+ ConstantsSQL.MINE_TTX + " text, " + ConstantsSQL.TEXT1 + " text, " + ConstantsSQL.TEXT2 + " text, "
				+ ConstantsSQL.TEXT3 + " text, " + ConstantsSQL.TEXT4 + " text, " + ConstantsSQL.TEXT5 + " text, "
				+ ConstantsSQL.TEXT6 + " text, " + ConstantsSQL.TEXT7 + " text, " + ConstantsSQL.TEXT8 + " text, "
				+ ConstantsSQL.TEXT9 + " text, " + ConstantsSQL.TEXT10 + " text" + ");");
		new SQLInitData(db, context);

	}
}
