package com.soft.mines.model.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.soft.mines.model.constants.Constants;
import com.soft.mines.model.constants.ConstantsSQL;

/**
 * @author Doc ������ ����� ��������� ���� �������� ���������� � N ������������
 *         ��������Os
 */
public class SQLInitData {
	private SQLiteDatabase database;

	/**
	 * @param database
	 * @param context
	 */
	public SQLInitData(SQLiteDatabase database, Context context) {
		this.database = database;

		execInitSQL("name_pfm_1",
				"pfm_1_1", "pfm_1_2", "pfm_1_3", "pfm_1_4", "pfm_1_5", "", "", "", "", "",
				"pfm_1_t",
				"russia", Constants.MINES_SOLDATS, "pfm_1_list", "",
				"text_image1_pfm_1", "text_image2_pfm_1", "text_image3_pfm_1", "text_image4_pfm_1", "text_image5_pfm_1", "", "", "", "", "");

		execInitSQL("name_mon_50",
				"mon_50_1", "mon_50_2", "mon_50_3", "mon_50_4", "mon_50_5", "mon_50_6", "", "", "",	"",
				"mon_50_t",
				"russia", Constants.MINES_SOLDATS, "mon_50_list", "",
				"text_image1_mon_50", "text_image1_mon_50", "text_image3_mon_50", "text_image4_mon_50", "text_image5_mon_50", "text_image6_mon_50", "", "", "", "");

		execInitSQL("name_tm62",
				"tm62_1", "tm62_2", "tm62_3", "tm62_4", "tm62_5", "", "", "", "", "",
				"tm62_t",
				"afghanistan", Constants.MINES_TANK, "tm62_list", "", 
				"text_image1_tm62", "text_image2_tm62",	"text_image3_tm62", "text_image4_tm62", "text_image5_tm62", "", "", "", "", "");

	}

	/**
	 * @param name
	 *            - ��� ��������
	 * @param image1
	 *            - �������� �������� (������������ � ������ ���������)
	 * @param image2
	 *            - �������� ��������
	 * @param image3
	 *            - �������� ��������
	 * @param image4
	 *            - �������� ��������
	 * @param image5
	 *            - �������� ��������
	 * @param image6
	 *            - �������� ��������
	 * @param image7
	 *            - �������� ��������
	 * @param image8
	 *            - �������� ��������
	 * @param image9
	 *            - �������� ��������
	 * @param image10
	 *            - �������� ��������
	 * @param image_test
	 *            - �������� �������� (������������ � �����)
	 * @param country
	 *            - �������� ����� �������������
	 * @param mine_type
	 *            - ��� �������� (Constants.MINES_SOLDATS -
	 *            ���������������,Constants.MINES_TANK - ��������������� )
	 * @param mine_info
	 *            - ���� ������� �������� (������������ � ������ ���������)
	 * @param mine_ttx
	 *            - ��������� ����...
	 * @param text_image1
	 *            - �������� ��������
	 * @param text_image2
	 *            - �������� ��������
	 * @param text_image3
	 *            - �������� ��������
	 * @param text_image4
	 *            - �������� ��������
	 * @param text_image5
	 *            - �������� ��������
	 * @param text_image6
	 *            - �������� ��������
	 * @param text_image7
	 *            - �������� ��������
	 * @param text_image8
	 *            - �������� ��������
	 * @param text_image9
	 *            - �������� ��������
	 * @param text_image10
	 *            - �������� ��������
	 */
	public void execInitSQL(String name, String image1, String image2, String image3, String image4, String image5,
			String image6, String image7, String image8, String image9, String image10, String image_test,
			String country, String mine_type, String mine_info, String mine_ttx, String text_image1,
			String text_image2, String text_image3, String text_image4, String text_image5, String text_image6,
			String text_image7, String text_image8, String text_image9, String text_image10) {

		database.execSQL("INSERT INTO " + ConstantsSQL.TABLE_MINE + " ( " + ConstantsSQL.NAME + ", "
				+ ConstantsSQL.IMAGE1 + ", " + ConstantsSQL.IMAGE2 + ", " + ConstantsSQL.IMAGE3 + ", "
				+ ConstantsSQL.IMAGE4 + ", " + ConstantsSQL.IMAGE5 + ", " + ConstantsSQL.IMAGE6 + ", "
				+ ConstantsSQL.IMAGE7 + ", " + ConstantsSQL.IMAGE8 + ", " + ConstantsSQL.IMAGE9 + ", "
				+ ConstantsSQL.IMAGE10 + ", " + ConstantsSQL.IMAGE_TEST + " , " + ConstantsSQL.COUNTRY + ", "
				+ ConstantsSQL.MINE_TYPE + ", " + ConstantsSQL.MINE_INFO + ", " + ConstantsSQL.MINE_TTX + ", "
				+ ConstantsSQL.TEXT1 + ", " + ConstantsSQL.TEXT2 + ", " + ConstantsSQL.TEXT3 + ", "
				+ ConstantsSQL.TEXT4 + ", " + ConstantsSQL.TEXT5 + ", " + ConstantsSQL.TEXT6 + ", "
				+ ConstantsSQL.TEXT7 + ", " + ConstantsSQL.TEXT8 + ", " + ConstantsSQL.TEXT9 + ", "
				+ ConstantsSQL.TEXT10 + ") values ( '" + name + "', '" + image1 + "', '" + image2 + "', '" + image3
				+ "', '" + image4 + "', '" + image5 + "', '" + image6 + "', '" + image7 + "', '" + image8 + "', '"
				+ image9 + "', '" + image10 + "', '" + image_test + "', '" + country + "', '" + mine_type + "', '"
				+ mine_info + "', '" + mine_ttx + "', '" + text_image1 + "', '" + text_image2 + "', '" + text_image3
				+ "', '" + text_image4 + "', '" + text_image5 + "', '" + text_image6 + "', '" + text_image7 + "', '"
				+ text_image8 + "', '" + text_image9 + "', '" + text_image10 + "' )");
	}

}
