package com.soft.mines.model.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.soft.mines.model.GetResources;
import com.soft.mines.model.constants.ConstantsSQL;
import com.soft.mines.model.constants.TestMasterConstant;

/**
 * 
 * @author Nickolai
 * @version 0.0.1
 */

public class TestMaster {

	private ArrayList<HashMap<String, String>> data;

	private final String LOG_TAG = "Test Master";
	private Context context;
	private ArrayList<HashMap<String, Object>> questions;
	// contains a sequence of questions that will be asked
	private ImageView imageView;

	private int testPosition = 0;
	private int testSize;

	private EditText answerEditText;

	int trueAnswers = 0;

	/**
	 * Конструктор
	 * 
	 * @param context
	 *            базоый контекст
	 * 
	 * @param data
	 *            ArrayList<HashMap<String, String>>
	 * @param imageView
	 *            Image View
	 * @param answerEditText
	 *            edit text
	 */
	public TestMaster(Context context, ArrayList<HashMap<String, String>> data,
			ImageView imageView, EditText answerEditText) {
		this.data = data;
		this.context = context;
		this.imageView = imageView;
		this.answerEditText = answerEditText;
		questions = new ArrayList<HashMap<String, Object>>();
	}

	ArrayList<Integer> sequenceOfQuestions = new ArrayList<Integer>();

	private boolean checkQuestionNumber(int questionNumber) {
		for (int pos = 0; pos < sequenceOfQuestions.size(); pos++) {
			if (sequenceOfQuestions.get(pos) == questionNumber) {
				return true;
			}
		}
		return false;
	}

	private void prepareRecords() {
		Log.d(LOG_TAG, "prepare records...");
		for (int i = 0; i < data.size(); i++) {
			String mineName = data.get(i).get(ConstantsSQL.NAME);
			Log.d(LOG_TAG, "Mine name: " + mineName);
			Drawable minePicture = GetResources.getDrawable(context, data
					.get(i).get(ConstantsSQL.IMAGE_TEST));
			if (mineName.length() != 0 && minePicture != null) {
				HashMap<String, Object> temp = new HashMap<String, Object>();
				temp.put(TestMasterConstant.MINE_NAME, mineName);
				temp.put(TestMasterConstant.MINE_DRAWABLE, minePicture);
				questions.add(temp);
			}
		}
		Log.d(LOG_TAG, "Records prepared.");
	}

	/**
	 * Создает тест
	 * 
	 * @param testSize
	 *            размер теста
	 */
	public void createTest(int testSize) {
		prepareRecords();
		this.testSize = testSize;
		while (sequenceOfQuestions.size() != testSize) {
			int questionNumber = new Random().nextInt(testSize);
			for (int i = 0; i < testSize; i++) {
				if (checkQuestionNumber(questionNumber)) {
					break;
				} else {
					// add question
					sequenceOfQuestions.add(questionNumber);
				}
			}
		}
	}

	/**
	 * Выводит картинку мины
	 * 
	 * @param questionNumber
	 *            номер вопроса
	 */
	private void askQuestion(int questionNumber) {
		imageView.setImageDrawable((Drawable) questions.get(
				sequenceOfQuestions.get(questionNumber)).get(
				TestMasterConstant.MINE_DRAWABLE));
	}

	/**
	 * Запускает тест
	 * 
	 * @return true если тест может быть запущен <br>
	 *         false - если тест не может быть запущен.
	 */
	public boolean startTest() {
		Log.d(LOG_TAG, "Starting test...");
		Log.d(LOG_TAG, "Sequence of questions:");
		System.out.println(sequenceOfQuestions);
		Log.d(LOG_TAG, "Test position - " + testPosition);
		Log.d(LOG_TAG, "Questions:");
		System.out.println(questions);
		if (!isNoMoreQuestions()) {
			Log.d(LOG_TAG, "Test started! Ask qestion number - " + testPosition);
			askQuestion(testPosition);
			return true;
		}
		// тест не может запуститься, нет вопросов!
		Log.d(LOG_TAG, "No more questions!");
		return false;
	}

	/**
	 * Возвращает оценку
	 * 
	 * @return mark;
	 */
	public double getMark() {
		System.out.println(Double.parseDouble(trueAnswers + "") + " / "
				+ testSize + " * 10 = "
				+ (Double.parseDouble(trueAnswers + "") / testSize) * 10);
		return (Double.parseDouble(trueAnswers + "") / testSize) * 10;
	}

	/**
	 * Возвращает кол-во правильных ответов
	 * 
	 * @return int
	 */
	public int getTrueAnswers() {
		return trueAnswers;
	}

	/**
	 * Устанавливает кол-во правильных ответов
	 * 
	 * @param trueAnswers
	 */
	public void setTrueAnswers(int trueAnswers) {
		this.trueAnswers = trueAnswers;
	}

	/**
	 * Переходит на следующий вопрос
	 * 
	 * @return true если это возможно false - переход невозможен
	 */

	public boolean nextQuestion() {
		Log.d(LOG_TAG, "Sequence of questions:");
		System.out.println(sequenceOfQuestions);
		testPosition++;
		Log.d(LOG_TAG, "Test position - " + testPosition);
		Log.d(LOG_TAG, "Questions:");
		System.out.println(questions);
		if (!isNoMoreQuestions()) {
			askQuestion(testPosition);
		} else {
			Log.d(LOG_TAG, "End test!"); // end of test
			return false;
		}
		Log.d(LOG_TAG, "Swiched to next");
		return true;
	}

	/**
	 * Проверяет есть еще вопросы или нет
	 * 
	 * @return true - если еще есть вопрос(ы) false - больше вопросов нет
	 */
	public boolean isNoMoreQuestions() {
		if (testPosition >= testSize) {
			return true;
		}
		return false;
	}

	/**
	 * Восстанавливет тест
	 */
	public void restoreTest() {
		askQuestion(testPosition);
	}

	/**
	 * Проверяет ответ на правильность
	 * 
	 * @return true - верно false - неверно
	 */
	public boolean checkAnswer() {
		Log.d(LOG_TAG, "User answer:" + answerEditText.getText().toString());
		Log.d(LOG_TAG,
				"True answer:"
						+ questions.get(sequenceOfQuestions.get(testPosition))
								.get(TestMasterConstant.MINE_NAME));
		String answer = answerEditText.getText().toString().trim();
		String trueAnswer = (String) questions
				.get(sequenceOfQuestions.get(testPosition))
				.get(TestMasterConstant.MINE_NAME).toString().trim();

		String clearAnswer = deleteChars(answer);
		String clearTrueAnswer = deleteChars(trueAnswer);

		Log.d(LOG_TAG, "Clear answer: " + clearAnswer);
		Log.d(LOG_TAG, "Clear true answer: " + clearTrueAnswer);

		if (!isNoMoreQuestions()) {
			if (clearAnswer.compareToIgnoreCase(clearTrueAnswer) == 0) {
				Log.d(LOG_TAG, "True!");
				Log.d(LOG_TAG, "Answer checked!");
				answerEditText.setError(null);
				trueAnswers++;
				answerEditText.setText("");
				return true; // answer is true
			}
		}

		Log.d(LOG_TAG, "False");
		Log.d(LOG_TAG, "Answer checked!");
		answerEditText.setError(null);
		answerEditText.setText("");
		return false; // false
	}

	protected static String deleteChars(String string) {
		String newString = "";
		for (int i = 0; i < string.length(); i++) {
			char letter = string.charAt(i);
			if ((letter != '-') && (letter != ' ') && (letter != '.')
					&& (letter != ',') && (letter != '/') && (letter != '*')) {
				newString += string.charAt(i);
			}
		}
		return newString;
	}

	/**
	 * Возвращает номер текущего вопроса
	 * 
	 * @return int
	 */
	public int getTestPosition() {
		return testPosition;
	}

	/**
	 * Устанавливает номер текущего вопроса
	 * 
	 * @param testPosition
	 *            int test position
	 */
	public void setTestPosition(int testPosition) {
		this.testPosition = testPosition;
	}

	/**
	 * возвращает последовательность вопросов
	 * 
	 * @return ArrayList<Integer>
	 */
	public ArrayList<Integer> getSequenceOfQuestions() {
		return sequenceOfQuestions;
	}

	/**
	 * Устанавливает последовательность вопросов
	 * 
	 * @param sequenceOfQuestions
	 *            ArrayList<Integer>
	 */
	public void setSequenceOfQuestions(ArrayList<Integer> sequenceOfQuestions) {
		this.sequenceOfQuestions = sequenceOfQuestions;
	}
}
