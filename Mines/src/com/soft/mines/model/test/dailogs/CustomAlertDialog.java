package com.soft.mines.model.test.dailogs;

import android.app.AlertDialog;
import android.content.Context;

/**
 * @author Nickolai
 * @version 0.0.1
 */
public class CustomAlertDialog {

	/**
	 * Create custom Alert Dialog
	 * 
	 * @param context
	 *            Activity context
	 * @param Title
	 *            String title dialog
	 * @param Message
	 *            String dialog message
	 * @return AlerDialog
	 */

	public static AlertDialog create(Context context, String Title, String Message) {
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle(Title);
		adb.setMessage(Message);
		adb.setPositiveButton("Ok", null);
		return adb.create();
	}
}
