package com.soft.mines.model.constants;

import com.soft.mines.model.SQL.SQLInitData;

/**
 * @author Doc
 * 
 */
public class Constants {

	/**
	 * ��������� ��� ����������� ���� ����: ���������������
	 * 
	 * @see SQLInitData
	 */
	public static final String MINES_SOLDATS = "mines_soldats";

	/**
	 * ��������� ��� ����������� ���� ����: ���������������
	 * 
	 * @see SQLInitData
	 */
	public static final String MINES_TANK = "mines_tank";

	/**
	 * ��������� ��� ����������� ���� ��� � ����������� �� �������
	 * 
	 * @see SQLInitData
	 */
	public static final String MINES_COUNTRY = "mines_country";
	/**
	 * ��������� ��� ����������� ���� ��� � ����������� �� �������
	 * 
	 * @see SQLInitData
	 */
	public static final String MINES_SEARCH = "mines_search";
	/**
	 * ��������� ��� ����������� ���� ��� � ����������� �� �������
	 * 
	 * @see SQLInitData
	 */
	public static final String MINES_COUNTRY_NAME = "mines_country_name";

	/**
	 * ��������� ��� �������� ������ ����� Activity`s
	 */
	public static final String DATA = "data";
	/**
	 * ��������� ��� �������� ������ ��� ����������
	 */
	public static final String SEARCH = "search";

	/**
	 * �������� ��� �������� ������ ����� Activity`s
	 */
	public static final int NUMIMAGE = 10;

}
