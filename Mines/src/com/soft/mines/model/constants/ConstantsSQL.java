package com.soft.mines.model.constants;

/**
 * @author Doc
 * 
 */
public class ConstantsSQL {
	/**
	 * db name
	 */
	public static final String DATABASE_NAME = "db_mine";
	/**
	 * db vertion
	 */
	public static final int DATABASE_VERSION = 3;
	/**
	 * table name
	 */
	public static final String TABLE_MINE = "table_mine";

	/**
	 * table field _id
	 */
	public static final String ID = "_id";
	/**
	 * table field name
	 */
	public static final String NAME = "name";
	/**
	 * table field image1
	 */
	public static final String IMAGE1 = "image1";
	/**
	 * table field image2
	 */
	public static final String IMAGE2 = "image2";
	/**
	 * table field image3
	 */
	public static final String IMAGE3 = "image3";
	/**
	 * table field image4
	 */
	public static final String IMAGE4 = "image4";
	/**
	 * table field image5
	 */
	public static final String IMAGE5 = "image5";
	/**
	 * table field image6
	 */
	public static final String IMAGE6 = "image6";
	/**
	 * table field image7
	 */
	public static final String IMAGE7 = "image7";
	/**
	 * table field image8
	 */
	public static final String IMAGE8 = "image8";
	/**
	 * table field image9
	 */
	public static final String IMAGE9 = "image9";
	/**
	 * table field image10
	 */
	public static final String IMAGE10 = "image10";

	/**
	 * table field text for image
	 */
	public static final String TEXT1 = "text1";
	/**
	 * table field text for image
	 */
	public static final String TEXT2 = "text2";
	/**
	 * table field text for image
	 */
	public static final String TEXT3 = "text3";
	/**
	 * table field text for image
	 */
	public static final String TEXT4 = "text4";
	/**
	 * table field text for image
	 */
	public static final String TEXT5 = "text5";
	/**
	 * table field text for image
	 */
	public static final String TEXT6 = "text6";
	/**
	 * table field text for image
	 */
	public static final String TEXT7 = "text7";
	/**
	 * table field text for image
	 */
	public static final String TEXT8 = "text8";
	/**
	 * table field text for image
	 */
	public static final String TEXT9 = "text9";
	/**
	 * table field text for image
	 */
	public static final String TEXT10 = "text10";
	/**
	 * table field image test
	 */
	public static final String IMAGE_TEST = "image_test";
	/**
	 * table field country
	 */
	public static final String COUNTRY = "country";
	/**
	 * table field type mine
	 */
	public static final String MINE_TYPE = "mine_text1";
	/**
	 * table field mine info
	 */
	public static final String MINE_INFO = "mine_text2";
	/**
	 * table field mine ttx
	 */
	public static final String MINE_TTX = "mine_text3";
}
