package com.soft.mines.model.constants;

/**
 * @author Nickolai
 * @version 0.0.4
 */
public class ConstantEndTestDialog {
	/**
	 * Contains result test mark
	 */
	public static final String END_TEST_DIALOG_MARK_EXTRA = "mark";
}
