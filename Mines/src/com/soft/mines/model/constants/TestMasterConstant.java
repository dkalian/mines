package com.soft.mines.model.constants;

/**
 * @author Nickolai
 * @version 0.0.1
 * 
 */
public class TestMasterConstant {
	/**
	 * filed mine_name contains the name of mine
	 */
	public static final String MINE_NAME = "mine_name";
	/**
	 * filed mine_drawable contains the mine picture
	 */
	public static final String MINE_DRAWABLE = "mine_drawable";
}
