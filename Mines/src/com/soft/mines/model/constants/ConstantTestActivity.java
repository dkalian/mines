package com.soft.mines.model.constants;

/**
 * @author Nickolai
 * v 0.0.1
 */
public class ConstantTestActivity {
	public static final String KEY_SEQUENCE_OF_QUESTIONS = "sequence_of_questions";
	public static final String KEY_TEST_POSITION = "test_position";
	public static final String KEY_TEST_TRUE_ANSWERS = "true_answers";
	public static final String KEY_END_TEST_DIALOG_IS_SHOWING = "end_test_dialog_is_showing";
	public static final String KEY_MINE_NAME_TEXT = "mine_name_text";
	public static final String KEY_ACCEPT_BUTTON_IS_ENABLED = "accept_is_enabled";
}
