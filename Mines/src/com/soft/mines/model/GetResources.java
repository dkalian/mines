package com.soft.mines.model;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.soft.mines.model.constants.Constants;


/**
 * @author Doc
 *
 */
public class GetResources {
	private static final int FIST_MINE = 0;

	
	/**
	 * @param context
	 * @param filename
	 * @return Drawable
	 */
	public static Drawable getDrawable(Context context, String filename) {
		try {
			int imageResource = context.getResources().getIdentifier(filename,
					"drawable", context.getPackageName());
			return context.getResources().getDrawable(imageResource);
		} catch (Exception e) {
			int imageResource = context.getResources().getIdentifier("no_foto",
					"drawable", context.getPackageName());
			return context.getResources().getDrawable(imageResource);
		}

	}


	/**
	 * @param context
	 * @param filename
	 * @return int
	 */
	public static int getDrawableInt(Context context, String filename) {
		return context.getResources().getIdentifier(filename, "drawable",
				context.getPackageName());

	}

	
	/**
	 * @param context
	 * @param resName
	 * @return String
	 */
	public static String getString(Context context, String resName) {
		String result = "";
		try {
			if (resName.length() > 0) {
				int stringResource = context.getResources().getIdentifier(
						resName, "string", context.getPackageName());
				result = context.getResources().getString(stringResource);
			}
		} catch (NotFoundException e) {
			// TODO hardcode	
			result = "Error!"; 
		}
		return result;
	}

	/**
	 * @param context
	 * @param mineInfo
	 * @return arrayList image link
	 */
	public static ArrayList<Integer> getPageViewImageInt(Context context,
			ArrayList<HashMap<String, String>> mineInfo) {

		ArrayList<Integer> galleryImage = new ArrayList<Integer>();
		for (int i = 1; i < Constants.NUMIMAGE; i++) {
			if (mineInfo.get(FIST_MINE).get("image" + i).length() > 0) {
				galleryImage.add(getDrawableInt(context, mineInfo
						.get(FIST_MINE).get("image" + i)));
			}
		}

		return galleryImage;
	}

	/**
	 * @param context
	 * @param mineInfo
	 * @return arrayList text array for PageView
	 */
	public static ArrayList<String> getPageViewTextInt(Context context,
			ArrayList<HashMap<String, String>> mineInfo) {

		ArrayList<String> textArray = new ArrayList<String>();
		for (int i = 1; i < Constants.NUMIMAGE; i++) {
			Log.i(null, mineInfo.get(FIST_MINE).get("text" + i));
			if (mineInfo.get(FIST_MINE).get("text" + i).length() > 0) {
				textArray.add(mineInfo.get(FIST_MINE).get("text" + i));
			}
		}

		return textArray;
	}

}