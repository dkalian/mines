package com.soft.mines.controller;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.SQL.SQLAdapter;
import com.soft.mines.model.constants.ConstantEndTestDialog;
import com.soft.mines.model.constants.ConstantTestActivity;
import com.soft.mines.model.test.TestMaster;

/**
 * @author Nickolai
 * @version 0.0.1
 */
public class TestActivity extends ActionBarActivity {
	// how many questions need to answer
	private final int TEST_SIZE = 3;

	private static final String LOG_TAG = "Test Activity";

	private ImageButton accept;
	private ImageView picture;
	private EditText mineName;
	private TextView answerIndicator;

	private SQLAdapter dbConnection;

	private TestMaster testMaster;

	Animation alphaAnimationShow;

	ActionBar actionBar;

	private String EMPTY_FIELD_ERROR;
	private String ANSWER_INDICATOR_TRUE;
	private String ANSWER_INDICATOR_FALSE;

	private MenuItem mCurrentPage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		initResourses();
		initTest();
		testMaster.startTest();
	}

	private void initResourses() {
		EMPTY_FIELD_ERROR = getResources().getString(
				R.string.test_activity_empty_field_error);
		ANSWER_INDICATOR_TRUE = getResources().getString(
				R.string.test_activity_answer_indicator_true);
		ANSWER_INDICATOR_FALSE = getResources().getString(
				R.string.test_activity_answer_indicator_false);

		accept = (ImageButton) findViewById(R.id.activity_test_button_accept);
		picture = (ImageView) findViewById(R.id.activity_test_mine_picture);
		mineName = (EditText) findViewById(R.id.activity_test_mine_name_edit_text);
		answerIndicator = (TextView) findViewById(R.id.activity_test_answer_indicator);

		alphaAnimationShow = AnimationUtils.loadAnimation(this,
				R.anim.activity_test_answer_indicator_alpha_show);
		alphaAnimationShow.setAnimationListener(animationShowListener);
		accept.setOnClickListener(buttonAcceptOnClickListener);
		accept.setEnabled(true);
		actionBar = getSupportActionBar();
	}

	private void initTest() {
		dbConnection = new SQLAdapter(getBaseContext());
		ArrayList<HashMap<String, String>> data = dbConnection.getAllData();
		testMaster = new TestMaster(getBaseContext(), data, picture, mineName);
		testMaster.createTest(TEST_SIZE);
		dbConnection.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.test, menu);
		mCurrentPage = menu.findItem(R.id.action_page_count);
		// mCurrentPage.setEnabled(false);
		if (testMaster != null) {
			mCurrentPage
			.setTitle(String.valueOf(testMaster.getTestPosition() + 1)
					+ "/" + String.valueOf(TEST_SIZE));
		} else { 
			mCurrentPage.setTitle("1/" + String.valueOf(TEST_SIZE));
		}
		return true;
	}

	AnimationListener animationShowListener = new AnimationListener() {

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			answerIndicator.setVisibility(View.VISIBLE);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationEnd(Animation animation) {
			answerIndicator.setVisibility(View.INVISIBLE);
			testMaster.nextQuestion();
			if (testMaster.isNoMoreQuestions()) {
				accept.setEnabled(false);
				showEndTestDialog();
			}
			mCurrentPage
					.setTitle(String.valueOf(testMaster.getTestPosition() + 1)
							+ "/" + String.valueOf(TEST_SIZE));
		}
	};

	private void showEndTestDialog() {
		long mark = Math.round(testMaster.getMark());
		startActivity(new Intent(getBaseContext(), EndTestDialog.class)
				.putExtra(ConstantEndTestDialog.END_TEST_DIALOG_MARK_EXTRA,
						mark));
		finish();
	}

	private boolean validateData() {
		if (mineName.getText().toString().length() == 0) {
			mineName.setError(EMPTY_FIELD_ERROR);
			return false;
		}
		return true;
	}

	android.view.View.OnClickListener buttonAcceptOnClickListener = new android.view.View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (validateData()) {
				if (testMaster.checkAnswer()) {
					answerIndicator.startAnimation(alphaAnimationShow);
					answerIndicator.setText(ANSWER_INDICATOR_TRUE);
					answerIndicator
							.setBackgroundResource(R.drawable.bg_text_view_green);
				} else {
					answerIndicator.startAnimation(alphaAnimationShow);
					answerIndicator.setText(ANSWER_INDICATOR_FALSE);
					answerIndicator
							.setBackgroundResource(R.drawable.bg_text_view_red);
				}
			}
		}
	};

	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		initTest();
		testMaster
				.setSequenceOfQuestions(savedInstanceState
						.getIntegerArrayList(ConstantTestActivity.KEY_SEQUENCE_OF_QUESTIONS));
		testMaster.setTestPosition(savedInstanceState
				.getInt(ConstantTestActivity.KEY_TEST_POSITION));
		testMaster.setTrueAnswers(savedInstanceState
				.getInt(ConstantTestActivity.KEY_TEST_TRUE_ANSWERS));
		mineName.setText(savedInstanceState
				.getString(ConstantTestActivity.KEY_MINE_NAME_TEXT));
		accept.setEnabled(savedInstanceState
				.getBoolean(ConstantTestActivity.KEY_ACCEPT_BUTTON_IS_ENABLED));
		testMaster.restoreTest();
		Log.d(LOG_TAG, "onRestoreInstanceState end");
	};

	protected void onSaveInstanceState(Bundle outState) {
		Log.d(LOG_TAG, "onSaveInstanceState...");
		Log.d(LOG_TAG, "Save test position: " + testMaster.getTestPosition());
		Log.d(LOG_TAG, "Save test mark: " + testMaster.getMark());
		outState.putIntegerArrayList(
				ConstantTestActivity.KEY_SEQUENCE_OF_QUESTIONS,
				testMaster.getSequenceOfQuestions());
		outState.putInt(ConstantTestActivity.KEY_TEST_POSITION,
				testMaster.getTestPosition());
		outState.putInt(ConstantTestActivity.KEY_TEST_TRUE_ANSWERS,
				testMaster.getTrueAnswers());
		outState.putString(ConstantTestActivity.KEY_MINE_NAME_TEXT, mineName
				.getText().toString());
		outState.putBoolean(ConstantTestActivity.KEY_ACCEPT_BUTTON_IS_ENABLED,
				accept.isEnabled());
		Log.d(LOG_TAG, "onSaveInstanceState end");
	};
}