package com.soft.mines.controller;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.constants.ConstantEndTestDialog;

/**
 * @author Nickolai
 * @version 0.0.4
 */
public class EndTestDialog extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_end_test_dialog);
		TextView message = (TextView) findViewById(R.id.end_test_dialog_message);
		Bundle bundle = getIntent().getExtras();
		long mark = bundle.getLong(ConstantEndTestDialog.END_TEST_DIALOG_MARK_EXTRA);
		String temp = message.getText().toString();
		message.setText(temp+=mark);
		Button ok = (Button) findViewById(R.id.end_test_dialog_button_ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.end_test_dialog, menu);
		return true;
	}

}
