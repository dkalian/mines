package com.soft.mines.controller;

import java.util.ArrayList;
import java.util.HashMap;

import uk.co.senab.photoview.PhotoViewAttacher;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.GetResources;
import com.soft.mines.model.SQL.SQLAdapter;
import com.soft.mines.model.adapter.MinePageAdapter;
import com.soft.mines.model.constants.Constants;

/**
 * @author Doc
 * 
 */
public class InfoActivity extends ActionBarActivity implements OnPageChangeListener {

	private String id;
	private ArrayList<Integer> sDrawables;
	private ArrayList<HashMap<String, String>> dataMine;
	private LayoutInflater inflater;
	private ArrayList<View> pages;
	private ArrayList<String> sText;
	private MenuItem mCurrentPage;
	private SQLAdapter database;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		id = intent.getExtras().getString(Constants.DATA);
		database = new SQLAdapter(getBaseContext());
		dataMine = database.getOneMineInfo(id);
		sDrawables = GetResources.getPageViewImageInt(this, dataMine);
		sText = GetResources.getPageViewTextInt(this, dataMine);
		inflater = LayoutInflater.from(this);
		pages = new ArrayList<View>();

		if (dataMine != null && dataMine.size() > 0) {
			for (int i = 0; i < sDrawables.size(); i++) {
				createPage(sDrawables.get(i), sText.get(i));
			}
		}
		MinePageAdapter pagerAdapter = new MinePageAdapter(pages);
		ViewPager viewPager = new ViewPager(this);
		viewPager.setAdapter(pagerAdapter);
		viewPager.setCurrentItem(0);
		viewPager.setOnPageChangeListener(this);
		setContentView(viewPager);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info, menu);
		mCurrentPage = menu.findItem(R.id.action_page_count);
		mCurrentPage.setEnabled(false);
		mCurrentPage.setTitle("1/" + String.valueOf(sDrawables.size()));
		return true;
	}

	private void createPage(int imageIntMine, String textMine) {
		View page = inflater.inflate(R.layout.activity_info, null);
		TextView textView = (TextView) page.findViewById(R.id.textImageInfo);
		ImageView mImageView = (ImageView) page.findViewById(R.id.photoMine);
		textView.setText(textMine);
		Drawable bitmap = getResources().getDrawable(imageIntMine);
		mImageView.setImageDrawable(bitmap);

		// The MAGIC happens here!
		PhotoViewAttacher mAttacher = new PhotoViewAttacher(mImageView);
		mAttacher.setScaleType(ScaleType.FIT_XY);
		mAttacher.setOnPhotoTapListener(new PhotoTapListener());
		pages.add(page);
	}

	private class PhotoTapListener implements OnPhotoTapListener {

		@SuppressWarnings("unused")
		@Override
		public void onPhotoTap(View view, float x, float y) {
			float xPercentage = x * 100f;
			float yPercentage = y * 100f;
		}
	}

	@Override
	public void onPageScrollStateChanged(int index) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int index) {

		mCurrentPage.setTitle(String.valueOf(index + 1) + "/" + String.valueOf(sDrawables.size()));

	}

	@Override
	protected void onDestroy() {
		database.close();
		super.onDestroy();
	}
}
