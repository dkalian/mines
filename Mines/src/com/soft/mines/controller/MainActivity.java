package com.soft.mines.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.soft.mines.R;
import com.soft.mines.model.adapter.MenuAdapter;
import com.soft.mines.model.constants.Constants;

/**
 * @author Doc
 * 
 */
public class MainActivity extends ActionBarActivity implements OnItemClickListener, OnQueryTextListener {
	private final static int mTank = 0;
	private final static int mPehota = 1;
	private final static int mCountry = 2;
	private final static int mTest = 3;
	private ListView ListMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ListMenu = (ListView) findViewById(R.id.listViewMainMenu);
		ListMenu.setAdapter(new MenuAdapter(getApplicationContext()));
		ListMenu.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		MenuItem searchItem = menu.findItem(R.id.search);
		SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		searchView.setQueryHint(getResources().getString(R.string.action_bar_Search_hint));
		searchView.setOnQueryTextListener(this);

		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch (position) {
		case mTank:
			startActivity(new Intent(getApplicationContext(), MineActivity.class).putExtra(Constants.DATA,
					Constants.MINES_TANK));
			break;
		case mPehota:
			startActivity(new Intent(getApplicationContext(), MineActivity.class).putExtra(Constants.DATA,
					Constants.MINES_SOLDATS));
			break;
		case mCountry:
			startActivity(new Intent(getApplicationContext(), CountryActivity.class));
			break;
		case mTest:
			startActivity(new Intent(getApplicationContext(), TestActivity.class));
			break;

		}
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String text) {
		startActivity(new Intent(getApplicationContext(), MineActivity.class).putExtra(Constants.DATA,
				Constants.MINES_SEARCH).putExtra(Constants.SEARCH, text));
		return false;
	}

}
