package com.soft.mines.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.SQL.SQLAdapter;
import com.soft.mines.model.adapter.MineAdapter;
import com.soft.mines.model.constants.Constants;

/**
 * @author Doc
 * 
 */
public class MineActivity extends ActionBarActivity implements OnItemClickListener {

	private ListView vListView;
	private SQLAdapter dbConnection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mine);
		dbConnection = new SQLAdapter(getBaseContext());
		vListView = (ListView) findViewById(R.id.listViewMine);
		Intent intent = getIntent();
		if (intent.getExtras().getString(Constants.DATA).equals(Constants.MINES_SOLDATS)) {
			vListView.setAdapter(new MineAdapter(getBaseContext(), dbConnection.getSoldatsMinesData()));
		}
		if (intent.getExtras().getString(Constants.DATA).equals(Constants.MINES_TANK)) {
			vListView.setAdapter(new MineAdapter(getBaseContext(), dbConnection.getTanksMinesData()));
		}
		if (intent.getExtras().getString(Constants.DATA).equals(Constants.MINES_COUNTRY)) {

			vListView.setAdapter(new MineAdapter(getBaseContext(), dbConnection.getAllMinesCountry(intent.getExtras()
					.getString(Constants.MINES_COUNTRY_NAME))));
		}
		if ((intent.getExtras().getString(Constants.DATA).equals(Constants.MINES_SEARCH))) {
			MineAdapter adapter = new MineAdapter(getBaseContext(), dbConnection.getAllData());

			// MAGIC code!!! Save to my memory!
			adapter.getFilter().filter(intent.getExtras().getString(Constants.SEARCH), new Filter.FilterListener() {
				public void onFilterComplete(int count) {
					if (count == 0) {
						TextView vNoResult = (TextView) findViewById(R.id.noResult);
						vNoResult.setVisibility(View.VISIBLE);
					}
				}
			});
			vListView.setAdapter(adapter);
		}

		vListView.setOnItemClickListener(this);

	}

	@Override
	protected void onDestroy() {
		dbConnection.close();
		super.onDestroy();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		TextView textUrlId = (TextView) view.findViewById(R.id.mineId);
		startActivity(new Intent(this, InfoActivity.class).putExtra(Constants.DATA, textUrlId.getText()));

	}

}
