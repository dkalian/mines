package com.soft.mines.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.soft.mines.R;
import com.soft.mines.model.SQL.SQLAdapter;
import com.soft.mines.model.adapter.CountryAdapter;
import com.soft.mines.model.constants.Constants;

/**
 * @author Doc
 * 
 */
public class CountryActivity extends ActionBarActivity {
	private ListView vListView;
	private SQLAdapter dbConnection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_country);
		dbConnection = new SQLAdapter(getBaseContext());
		vListView = (ListView) findViewById(R.id.listViewMine);
		vListView.setAdapter(new CountryAdapter(getBaseContext(), dbConnection.getCounnryMinesSort()));
		vListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView textUrlId = (TextView) view.findViewById(R.id.mineId);
				startActivity(new Intent(CountryActivity.this, MineActivity.class).putExtra(Constants.DATA,
						Constants.MINES_COUNTRY).putExtra(Constants.MINES_COUNTRY_NAME, textUrlId.getText()));

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.country, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		dbConnection.close();
		super.onDestroy();
	}
}
